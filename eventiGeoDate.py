#!/usr/bin/env python2.7

# coding: utf-8

# In[96]:


import json, requests, urllib, time
import pandas as pd
import geocoder
from ConfigParser import SafeConfigParser


# In[97]:


parser = SafeConfigParser()
parser.read('geo.ini')
apiOC=parser.get('opencage', 'API')
apiWitAi=parser.get('witai', 'API')
webfolderpath=parser.get('webfolder', 'path')


# In[98]:


def convertiInData( str ):
    """"Converta in data, tramite wit.it la stringa passata"""
    # encoding della stringa prima di inviarla come argomento della query
    data=urllib.quote_plus(str)
    url = 'https://api.wit.ai/message?q='+data
    headers = {'Authorization': 'Bearer '+apiWitAi}
    r = requests.get(url, headers=headers)
    resp = r.json()
    tipo=resp['entities']['datetime'][0]['type']
    if tipo == "value":
        dataV=resp['entities']['datetime'][0]['value']
        dataFrom=""
        dataTo=""
    elif tipo == "interval":
        dataV=""
        dataFrom=resp['entities']['datetime'][0]['from']['value']
        dataTo=resp['entities']['datetime'][0]['to']['value']
    return tipo+','+dataV+','+dataFrom+','+dataTo


# In[99]:


df=pd.read_csv("http://dev.ondata.it/projs/opendatasicilia/eventiBeniCulturaliRegioneSiciliana/eventi.csv")


# In[100]:


df['dataRaw']=df['data'].apply(convertiInData)


# In[101]:


dateDf=pd.DataFrame(df.dataRaw.str.split(',',3).tolist(),columns = ['tipoData','dataValue','dataFrom','dataTo'])


# In[102]:


df=pd.concat([df,dateDf],axis=1)


# In[103]:


df.drop(['dataRaw'], axis=1, inplace=True)


# In[104]:


df.iloc[:,25:28] = df.iloc[:,25:28].apply(pd.to_datetime, errors='coerce')


# In[105]:


# gli eventi in corso
eventiInCorso=df[(df['dataTo'] > time.strftime("%Y-%m-%d")) | (df['dataValue'] > time.strftime("%Y-%m-%d"))].sort_values('dataTo',ascending=True)[['titolo','citta','dataFrom','dataTo','URL']]


# In[106]:


eventiInCorso.to_csv(webfolderpath+'/eventiInCorso.csv',encoding='utf-8',index=False)


# In[108]:


def geoinfo(citta):
    g = geocoder.opencage(citta, key=apiOC)
    output=g.city+','+str(g.lat)+','+str(g.lng)
    return output


# In[109]:


df['2geo']=df['citta']+','+df['provincia']
df['geoRaw']=df['2geo'].apply(geoinfo)


# In[110]:


geoDf=pd.DataFrame(df.geoRaw.str.split(',',2).tolist(),columns = ['cittaGeo','latitude','longitude'])
df=pd.concat([df,geoDf],axis=1)
df.drop(['2geo','geoRaw'], axis=1, inplace=True)


# In[111]:


df.to_csv(webfolderpath+'/eventiGeoDate.csv',encoding='utf-8',index=False)

