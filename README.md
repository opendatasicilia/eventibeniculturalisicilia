# Descrizione

Il file [**dw.sh**](./dw.sh) è uno script `bash` che scarica i dati degli eventi presenti qui [http://www.regione.sicilia.it/beniculturali/sitepda/eventi_pda.asp?page=1#primo](http://www.regione.sicilia.it/beniculturali/sitepda/eventi_pda.asp?page=1#primo) e li converte in `JSON` e `CSV`.

Gli URL pubblici dei file sono:

- [http://dev.ondata.it/projs/opendatasicilia/eventiBeniCulturaliRegioneSiciliana/eventi.json](http://dev.ondata.it/projs/opendatasicilia/eventiBeniCulturaliRegioneSiciliana/eventi.json)
- [http://dev.ondata.it/projs/opendatasicilia/eventiBeniCulturaliRegioneSiciliana/eventi.csv](http://dev.ondata.it/projs/opendatasicilia/eventiBeniCulturaliRegioneSiciliana/eventi.csv)

Su data.world viene aggiornato automaticamente questo [dataset](https://data.world/aborruso/eventi-beni-culturali-sicilia).

I dati sono **aggiornati ogni notte**.

## Requisiti dello script bash

- jq [https://github.com/stedolan/jq](https://github.com/stedolan/jq);
- csvkit [https://csvkit.readthedocs.io](https://csvkit.readthedocs.io);
- tidy [http://tidy.sourceforge.net/docs/tidy_man.html](http://tidy.sourceforge.net/docs/tidy_man.html);
- pup [https://github.com/ericchiang/pup](https://github.com/ericchiang/pup);

## Esempio di grafico

Le dimensioni sono proporzionali al numero di eventi per provincia.

![chart](https://data.world/api/chart/export/98ba153fb696bb8af795f977744c57a3970dafb890329fa2dbc8c91a9c34a018.png)