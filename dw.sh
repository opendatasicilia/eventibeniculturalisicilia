#!/bin/bash

# Imposto la cartella di lavoro leggendo il PATH assoluto di questo script.
# Tutto è relativo quindi a questo file.
cartella="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Leggo l'API Key di dataworld
source "$cartella"/.api

# Leggo alcuni parametri di configurazione: ad esempio uno dei percorsi di output (la variabile $output)
source "$cartella"/.config

# creo la cartella json (solo se non esiste)
mkdir -p "$cartella"/json

# Svuoto la cartella di lavoro dai vecchi file prodotti
rm "$cartella"/URL.txt > /dev/null 2>&1
rm "$cartella"/json/*.json > /dev/null 2>&1

# Apro la pagina sorgente e leggo il codice HTTP di risposta
code=$(curl -s -o /dev/null -w "%{http_code}" 'http://www.regione.sicilia.it/beniculturali/sitepda/eventi_pda.asp?page=1#primo' \
-H 'Accept-Encoding: gzip, deflate' -H 'Language: it,en-US;q=0.8,en;q=0.6' -H 'Upgrade-Insecure-Requests: 1' \
-H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.31 Safari/537.36' \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
-H 'Referer: http://93.94.88.173/castelbuono/mc/mc_p_ricerca.php?x=' -H 'Connection: keep-alive')

# Se il codice di risposta è `200`, tutto ok e procedo
if [ $code -eq 200 ]
then
	curl -sL "http://www.regione.sicilia.it/beniculturali/sitepda/eventi_pda.asp?page=1#primo" > "$cartella"/pagina.html

	# estraggo il numero di item
	numero=$(<"$cartella"/pagina.html grep -oE "Trovati: .*?cie" | sed -r 's/^(.*: )([0-9]{1,5})(.*)$/\2/g')
	# dal numero di item estraggo il numero di pagine
	pagine=$(python -c "import math; print(int(math.ceil(float($numero)/10)))")

	# estraggo gli URL degli eventi di ogni pagina
	for i in $(seq 1 $pagine); do
		curl -sL "http://www.regione.sicilia.it/beniculturali/sitepda/eventi_pda.asp?page=$i#primo" | \
		pup 'a[class="aaa"] attr{href}' >> "$cartella"/URL.txt
	done

	# Estraggo con scrape e jq gli attributi di ogni evento. Prima di estrarre i dati faccio un po' di pulizia con tidy, perché l'HTML originale è brutto
	# Aggiungo come attributo al JSON l'ID e l'URL della pagina
	while read p; do
	  nomefile=$(echo $p |  sed -r 's/^(.*?cod=)(.*)$/\2/g')
	  curl -sL "$p" | tidy -q --show-warnings no --drop-proprietary-attributes y --show-errors 0 --force-output y | \
	  scrape -be "//table" | xml2json | \
	  jq '[.html.body.table | {citta:.tr[3].td[2]."$t", provincia:.tr[5].td[2]."$t",luogo:.tr[7].td[2]."$t",indirizzo:.tr[9].td[2]."$t",telefono:.tr[11].td[2]."$t",fax:.tr[13].td[2]."$t",email:.tr[15].td[2]."$t",img:.tr[17].td.img.src,titolo:.tr[20].td[2].strong."$t",tipologia:.tr[22].td[2]."$t",descrizione:.tr[24].td[2]."$t",data:.tr[26].td[2]."$t",orari:.tr[28].td[2]."$t",informazioni:.tr[30].td[2]."$t",agevolazioni:.tr[32].td[2]."$t",finanziamento:.tr[34].td[2]."$t",altro:.tr[36].td[2]."$t",note:.tr[38].td[2]."$t",link1:.tr[40].td[2].a.href?,link2:.tr[42].td[2].a.href?,allegato1:.tr[44].td[2].a.href?,allegato2:.tr[44].td[2].a.href?}] | .[] |= .+ {"id":'"$nomefile"',"URL":"'"$p"'"}' \
	  > "$cartella/json/$nomefile".json
	done <"$cartella"/URL.txt

	# faccio il merge dei json di output in un unico file
	jq -s add "$cartella"/json/*.json > "$cartella"/eventi_raw.json
	
	# rimuovo dei caratteri non stampabili	
	sed -i -r  's/(\o302\o240){1,10}/ /g' "$cartella"/eventi_raw.json

	# ordino il JSON per id decresente
	< "$cartella"/eventi_raw.json jq '.|=sort_by(-.id)' > "$cartella"/eventi.json

	# faccio un po' di pulizia nel json nei campi che contengono valori null espressi come ".", "-", ecc.
	sed -i -r 's/"http:.."/null/g ; s/".",/null,/g ; s/( ){2,10}/ /g' "$cartella"/eventi.json

	# converto il json in CSV
	cat "$cartella"/eventi.json | in2csv --no-inference  -f json > "$cartella"/eventi.csv

	cp "$cartella"/eventi.csv "$output"/eventi.csv
	cp "$cartella"/eventi.json "$output"/eventi.json

	python "$cartella"/eventiGeoDate.py
	
	# lancio il sync su dataworld
	curl --request POST \
	  --url https://api.data.world/v0/datasets/aborruso/eventi-beni-culturali-sicilia/sync \
	  --header 'authorization: Bearer '"$dataworldAPI"'' \
	  --data '{}'
	
fi
